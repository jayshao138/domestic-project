package com.ruoyi.domestic;

import com.ruoyi.domestic.mapper.ServerUserMapper;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @author JayerListen
 * @create 2023-04-03 2023/4/3
 */
@SpringBootTest
public class test1 {
    @Autowired
    private ServerUserMapper serverUserMapper;

    //查全部
    @Test
    public void getAll(){
        List<SysUser> list = serverUserMapper.selectUserList(null,5);
        System.out.println(list);
    }
}
