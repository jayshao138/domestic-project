package com.ruoyi.domestic.mapper;

import java.util.List;
import com.ruoyi.domestic.domain.Work;

/**
 * 服务Mapper接口
 * 
 * @author JayerListen
 * @date 2023-04-03
 */
public interface WorkMapper 
{
    /**
     * 查询服务
     * 
     * @param workId 服务主键
     * @return 服务
     */
    public Work selectWorkByWorkId(Long workId);

    /**
     * 查询服务列表
     * 
     * @param work 服务
     * @return 服务集合
     */
    public List<Work> selectWorkList(Work work);

    /**
     * 新增服务
     * 
     * @param work 服务
     * @return 结果
     */
    public int insertWork(Work work);

    /**
     * 修改服务
     * 
     * @param work 服务
     * @return 结果
     */
    public int updateWork(Work work);

    /**
     * 删除服务
     * 
     * @param workId 服务主键
     * @return 结果
     */
    public int deleteWorkByWorkId(Long workId);

    /**
     * 批量删除服务
     * 
     * @param workIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWorkByWorkIds(Long[] workIds);
}
