package com.ruoyi.domestic.service.impl;

import java.util.List;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.domestic.mapper.OrderMapper;
import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.service.IOrderService;

/**
 * 订单-家政服务Service业务层处理
 * 
 * @author jayerListen
 * @date 2023-04-10
 */
@Service
public class OrderServiceImpl implements IOrderService 
{
    @Autowired
    private OrderMapper orderMapper;

    /**
     * 查询订单-家政服务
     * 
     * @param orderId 订单-家政服务主键
     * @return 订单-家政服务
     */
    @Override
    public Order selectOrderByOrderId(Long orderId)
    {
        return orderMapper.selectOrderByOrderId(orderId);
    }

    /**
     * 查询订单-家政服务列表
     * 
     * @param order 订单-家政服务
     * @return 订单-家政服务
     */
    @Override
    public List<Order> selectOrderList(Order order)
    {
        return orderMapper.selectOrderList(order);
    }

    /**
     * 新增订单-家政服务
     * 
     * @param order 订单-家政服务
     * @return 结果
     */
    @Override
    public int insertOrder(Order order)
    {
        order.setCreateTime(DateUtils.getNowDate());
        return orderMapper.insertOrder(order);
    }

    /**
     * 修改订单-家政服务
     * 
     * @param order 订单-家政服务
     * @return 结果
     */
    @Override
    public int updateOrder(Order order)
    {
        order.setUpdateTime(DateUtils.getNowDate());
        return orderMapper.updateOrder(order);
    }

    /**
     * 批量删除订单-家政服务
     * 
     * @param orderIds 需要删除的订单-家政服务主键
     * @return 结果
     */
    @Override
    public int deleteOrderByOrderIds(Long[] orderIds)
    {
        return orderMapper.deleteOrderByOrderIds(orderIds);
    }

    /**
     * 删除订单-家政服务信息
     * 
     * @param orderId 订单-家政服务主键
     * @return 结果
     */
    @Override
    public int deleteOrderByOrderId(Long orderId)
    {
        return orderMapper.deleteOrderByOrderId(orderId);
    }
}
