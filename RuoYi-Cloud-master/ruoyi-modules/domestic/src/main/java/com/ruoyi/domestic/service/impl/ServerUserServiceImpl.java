package com.ruoyi.domestic.service.impl;

import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.domain.BaseEntity;
import com.ruoyi.domestic.Feigh.SysUserFeigh;
import com.ruoyi.domestic.domain.ServerUser;
import com.ruoyi.domestic.domain.ServerWork;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.domestic.domain.vo.ServerUserVo;
import com.ruoyi.domestic.mapper.ServerUserMapper;
import com.ruoyi.domestic.service.IServerUserService;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.domain.SysUserRole;
import com.ruoyi.system.mapper.SysUserMapper;
import com.ruoyi.system.service.ISysUserService;
import io.seata.spring.annotation.GlobalTransactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JayerListen
 * @create 2023-04-03 2023/4/3
 */
@Service
public class ServerUserServiceImpl implements IServerUserService {
    private static final Logger log = LoggerFactory.getLogger(ServerUserServiceImpl.class);

    @Autowired
    private ServerUserMapper serverUserMapper;
    @Autowired
    private SysUserFeigh sysUserFeigh;

    @Override
    public List<ServerUserVo> selectUserList(SysUser user, int workId) {
        return serverUserMapper.selectUserList(user,workId);
    }

    @Override
    @GlobalTransactional
    public int insertServer(SysUser user) {
        SysUser server = sysUserFeigh.insertServer(user);
//        int ii=1/0;
        if(server!=null){
            int i = serverUserMapper.insertServerUser(server);
            if(i!=0) return 1;
        }
        return 0;
    }

    @Override
    public ServerUser selectServerUserById(Long userId) {
        return serverUserMapper.selectServerUserById(userId);
    }

    @Override
    public List<Work> selectWorksOfServer(Long serverId) {
        return serverUserMapper.selectWorksOfServer(serverId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateWorkOfServer(Long serverId,Long[] workIds) {
        serverUserMapper.deleteServerWorkByServerId(serverId);
        insertServerWork(serverId,workIds);
    }
    public void insertServerWork(Long serverId,Long[] workIds)
    {
        if (StringUtils.isNotEmpty(workIds))
        {
            // 新增用户与角色管理
            List<ServerWork> list = new ArrayList<ServerWork>();
            for (Long workId : workIds)
            {
                ServerWork sw=new ServerWork();
                sw.setServerId(serverId);
                sw.setWorkId(workId);
                list.add(sw);
            }
            serverUserMapper.batchServerWork(list);
        }
    }
}
