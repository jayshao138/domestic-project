package com.ruoyi.domestic.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.domestic.service.IServerDemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domestic.domain.Demand;
import com.ruoyi.domestic.service.IDemandService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 需求Controller
 *
 * @author jayerListen
 * @date 2023-04-10
 */
@RestController
@RequestMapping("/serverdemand")
public class ServerDemandController extends BaseController
{
    @Autowired
    private IServerDemandService serverdemandService;

    /**
     * 查询需求列表
     */
    @RequiresPermissions("domestic:serverdemand:list")
    @GetMapping("/list")
    public TableDataInfo list(Demand demand)
    {
        startPage();
        List<Demand> list = serverdemandService.selectDemandList(demand);
        return getDataTable(list);
    }

    @RequiresPermissions("domestic:serverdemand:recieveDemand")
    @PostMapping("/recieveDemand/{demandId}")
    public AjaxResult recieveDemand(@PathVariable("demandId") Long demandId)
    {
        return toAjax(serverdemandService.updateDemand(demandId));
    }

    @RequiresPermissions("domestic:serverdemand:list")
    @GetMapping("/mylist")
    public TableDataInfo Serverlist(Demand demand)
    {
        startPage();
        List<Demand> list = serverdemandService.selectDemandByServerId(demand);
        return getDataTable(list);
    }

}
