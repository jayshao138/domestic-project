package com.ruoyi.domestic.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domestic.domain.DataShower;
import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.mapper.DataShowMapper;
import com.ruoyi.domestic.service.DataShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author JayerListen
 * @create 2023-04-15 2023/4/15
 */
@RestController
@RequestMapping("/dataShow")
public class DataShowController extends BaseController {

    @Autowired
    private DataShowService dataShowService;

    @RequiresPermissions("domestic:data:show")
    @GetMapping("/system")
    public AjaxResult datashow()
    {
        DataShower datashow = dataShowService.datashow();
        return success(datashow);
    }

    @RequiresPermissions("domestic:data:show")
    @GetMapping("/client")
    public AjaxResult datashowOfClient()
    {
        DataShower dataShower = dataShowService.datashowOfClient();
        return success(dataShower);
    }

    @RequiresPermissions("domestic:data:show")
    @GetMapping("/server")
    public AjaxResult datashowOfServer()
    {
        DataShower dataShower = dataShowService.datashowOfServer();
        return success(dataShower);
    }
}
