package com.ruoyi.domestic.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domestic.domain.Demand;
import com.ruoyi.domestic.service.IClientSubmitDemandService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author JayerListen
 * @create 2023-04-07 2023/4/7
 */
@RestController
@RequestMapping("/clientSubmitDemand")
public class ClientSubmitDemandController extends BaseController {
    @Autowired
    private IClientSubmitDemandService clientSubmitDemandService;
    /**
     * 用户发布需求
     */
    @RequiresPermissions("domestic:submitDemand:submit")
    @PostMapping
    public AjaxResult submitDemand(@RequestBody Demand demand)
    {
        return toAjax(clientSubmitDemandService.submitDemand(demand.getWorkId(),demand.getWorkName(),demand.getDemandDetail()));
    }
}
