package com.ruoyi.domestic.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * @author JayerListen
 * @create 2023-04-03 2023/4/3
 */
public class ServerUser extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long serverId;

    /** 用户id */
    private Long userId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Server{" +
                "serverId=" + serverId +
                ", userId=" + userId +
                '}';
    }
}
