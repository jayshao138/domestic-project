package com.ruoyi.domestic.mapper;

import com.ruoyi.domestic.domain.Order;

import java.util.List;

public interface ClientOrderMapper {
    public List<Order> selectOrderByClientId(Order order);

}
