package com.ruoyi.domestic.domain;

import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * @author JayerListen
 * @create 2023-04-15 2023/4/15
 */
public class DataShower extends BaseEntity {
    private static final long serialVersionUID = 1L;
    //管理员端
    private Long icon_data_shop;
    private Long icon_data_corporation;
    private Long icon_data_nodingdan;
    private Long icon_data_user;
    //用户端
    private Long icon_data_yiquxiaodingdan;
    private Long icon_data_yifabudingdan;
    private Long icon_data_alldingdan;
    private Long icon_data_daijieshoudingdan;
    //员工端
//    private Long icon_data_daijieshoudingdan;
    private Long icon_data_finishdingdan;
//    private Long icon_data_nodingdan;
//    private Long icon_data_alldingdan;

    public Long getIcon_data_shop() {
        return icon_data_shop;
    }

    public void setIcon_data_shop(Long icon_data_shop) {
        this.icon_data_shop = icon_data_shop;
    }

    public Long getIcon_data_corporation() {
        return icon_data_corporation;
    }

    public void setIcon_data_corporation(Long icon_data_corporation) {
        this.icon_data_corporation = icon_data_corporation;
    }

    public Long getIcon_data_nodingdan() {
        return icon_data_nodingdan;
    }

    public void setIcon_data_nodingdan(Long icon_data_nodingdan) {
        this.icon_data_nodingdan = icon_data_nodingdan;
    }

    public Long getIcon_data_user() {
        return icon_data_user;
    }

    public void setIcon_data_user(Long icon_data_user) {
        this.icon_data_user = icon_data_user;
    }

    public Long getIcon_data_yiquxiaodingdan() {
        return icon_data_yiquxiaodingdan;
    }

    public void setIcon_data_yiquxiaodingdan(Long icon_data_yiquxiaodingdan) {
        this.icon_data_yiquxiaodingdan = icon_data_yiquxiaodingdan;
    }

    public Long getIcon_data_yifabudingdan() {
        return icon_data_yifabudingdan;
    }

    public void setIcon_data_yifabudingdan(Long icon_data_yifabudingdan) {
        this.icon_data_yifabudingdan = icon_data_yifabudingdan;
    }

    public Long getIcon_data_alldingdan() {
        return icon_data_alldingdan;
    }

    public void setIcon_data_alldingdan(Long icon_data_alldingdan) {
        this.icon_data_alldingdan = icon_data_alldingdan;
    }

    public Long getIcon_data_daijieshoudingdan() {
        return icon_data_daijieshoudingdan;
    }

    public void setIcon_data_daijieshoudingdan(Long icon_data_daijieshoudingdan) {
        this.icon_data_daijieshoudingdan = icon_data_daijieshoudingdan;
    }

    public Long getIcon_data_finishdingdan() {
        return icon_data_finishdingdan;
    }

    public void setIcon_data_finishdingdan(Long icon_data_finishdingdan) {
        this.icon_data_finishdingdan = icon_data_finishdingdan;
    }

    @Override
    public String toString() {
        return "DataShower{" +
                "icon_data_shop=" + icon_data_shop +
                ", icon_data_corporation=" + icon_data_corporation +
                ", icon_data_nodingdan=" + icon_data_nodingdan +
                ", icon_data_user=" + icon_data_user +
                ", icon_data_yiquxiaodingdan=" + icon_data_yiquxiaodingdan +
                ", icon_data_yifabudingdan=" + icon_data_yifabudingdan +
                ", icon_data_alldingdan=" + icon_data_alldingdan +
                ", icon_data_daijieshoudingdan=" + icon_data_daijieshoudingdan +
                ", icon_data_finishdingdan=" + icon_data_finishdingdan +
                '}';
    }
}
