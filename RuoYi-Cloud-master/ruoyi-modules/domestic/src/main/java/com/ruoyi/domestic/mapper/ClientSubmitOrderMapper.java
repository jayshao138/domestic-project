package com.ruoyi.domestic.mapper;

import com.ruoyi.domestic.domain.Order;

/**
 * @author ll
 * @create 2023-04-09 2023/4/9
 */

public interface ClientSubmitOrderMapper {
    /**
     * 用户下订单
     *
     * @param demand 需求demain
     * @return 结果
     */
    public int insertOrder(Order order);
}