package com.ruoyi.domestic.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.domestic.mapper.DemandMapper;
import com.ruoyi.domestic.domain.Demand;
import com.ruoyi.domestic.service.IDemandService;

/**
 * 需求Service业务层处理
 * 
 * @author jayerListen
 * @date 2023-04-10
 */
@Service
public class DemandServiceImpl implements IDemandService 
{
    @Autowired
    private DemandMapper demandMapper;

    /**
     * 查询需求
     * 
     * @param demandId 需求主键
     * @return 需求
     */
    @Override
    public Demand selectDemandByDemandId(Long demandId)
    {
        return demandMapper.selectDemandByDemandId(demandId);
    }

    /**
     * 查询需求列表
     * 
     * @param demand 需求
     * @return 需求
     */
    @Override
    public List<Demand> selectDemandList(Demand demand)
    {
        return demandMapper.selectDemandList(demand);
    }

    /**
     * 新增需求
     * 
     * @param demand 需求
     * @return 结果
     */
    @Override
    public int insertDemand(Demand demand)
    {
        return demandMapper.insertDemand(demand);
    }

    /**
     * 修改需求
     * 
     * @param demand 需求
     * @return 结果
     */
    @Override
    public int updateDemand(Demand demand)
    {
        return demandMapper.updateDemand(demand);
    }

    /**
     * 批量删除需求
     * 
     * @param demandIds 需要删除的需求主键
     * @return 结果
     */
    @Override
    public int deleteDemandByDemandIds(Long[] demandIds)
    {
        return demandMapper.deleteDemandByDemandIds(demandIds);
    }

    /**
     * 删除需求信息
     * 
     * @param demandId 需求主键
     * @return 结果
     */
    @Override
    public int deleteDemandByDemandId(Long demandId)
    {
        return demandMapper.deleteDemandByDemandId(demandId);
    }
}
