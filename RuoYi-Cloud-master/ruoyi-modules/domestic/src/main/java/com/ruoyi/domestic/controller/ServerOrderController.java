package com.ruoyi.domestic.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domestic.domain.Demand;
import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.service.IClientSubmitOrderService;
import com.ruoyi.domestic.service.IServerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/serverOrder")
public class ServerOrderController  extends BaseController {
    @Autowired
    private IServerOrderService serverOrderService;

    @RequiresPermissions("domestic:serverOrder:getMyOrders")
    @GetMapping("/getMyOrders")
    public TableDataInfo getMyOrders(Order order)
    {
        startPage();
        List<Order> orders = serverOrderService.selectServerOrderByServerId(order);
        return getDataTable(orders);
    }

    @RequiresPermissions("domestic:serverOrder:edit")
    @PostMapping("/confirmOrder/{orderId}")
    public AjaxResult confirmOrder(@PathVariable("orderId") Long orderId)
    {
        return toAjax(serverOrderService.confirmOrder(orderId));
    }

    @RequiresPermissions("domestic:serverOrder:edit")
    @PostMapping("/startWork/{orderId}")
    public AjaxResult startWork(@PathVariable("orderId") Long orderId)
    {
        return toAjax(serverOrderService.startWorkOfOrder(orderId));
    }

    @RequiresPermissions("domestic:serverOrder:edit")
    @PostMapping("/endWork/{orderId}")
    public AjaxResult endWork(@PathVariable("orderId") Long orderId)
    {
        return toAjax(serverOrderService.endWorkOfOrder(orderId));
    }

    @RequiresPermissions("domestic:serverOrder:edit")
    @PostMapping("/cancelOrder/{orderId}")
    public AjaxResult cancelOrder(@PathVariable("orderId") Long orderId)
    {
        return toAjax(serverOrderService.cancelOrder(orderId));
    }

}
