package com.ruoyi.domestic.controller;

import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.domestic.Feigh.SysUserFeigh;
import com.ruoyi.domestic.domain.ServerUser;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.domestic.domain.vo.ServerUserVo;
import com.ruoyi.domestic.service.IServerUserService;
import com.ruoyi.domestic.service.IWorkService;
import com.ruoyi.system.api.domain.SysDept;
import com.ruoyi.system.api.domain.SysRole;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.service.ISysUserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author JayerListen
 * @create 2023-04-03 2023/4/3
 */
@RestController
@RequestMapping("/server")
public class ServerUserController extends BaseController {
    @Autowired
    private IServerUserService serverUserService;
    @Autowired
    private IWorkService workService;
    @Autowired
    private SysUserFeigh sysUserFeigh;
    /**
     * 获取部门树列表
     */
    @RequiresPermissions("domestic:server:list")
    @GetMapping("/workTree")
    public AjaxResult deptTree(Work work)
    {
        return success(workService.selectWorkTreeList(work));
    }

    /**
     * 获取服务人列表
     */
    @RequiresPermissions("domestic:server:list")
    @GetMapping("/list")
    public TableDataInfo list(SysUser user, @Param("workId")int workId)
    {
        startPage();
        List<ServerUserVo> list = serverUserService.selectUserList(user,workId);
        return getDataTable(list);
    }

    /**
     * 新增服务人
     */
    @RequiresPermissions("system:user:add")
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysUser user)
    {
        return toAjax(serverUserService.insertServer(user));
    }

    /**
     * 根据服务人编号获取服务项目
     */
    @RequiresPermissions("system:user:query")
    @GetMapping("/allocWork/{userId}")
    public AjaxResult getWorksOfServer(@PathVariable("userId") Long userId)
    {
        AjaxResult ajax = AjaxResult.success();
        SysUser user = sysUserFeigh.getUser(userId);
        ServerUser serverUser = serverUserService.selectServerUserById(userId);
        List<Work> works = serverUserService.selectWorksOfServer(serverUser.getServerId());
        List<Work> workListOld = workService.selectWorkList(new Work());
        List<Work> workListNew = new ArrayList<Work>();
        for(Work work:workListOld){
            if(work.getParentId()!=0) workListNew.add(work);
        }
        ajax.put("user",user);
        ajax.put("works",works);
        ajax.put("workList",workListNew);
//        List<SysRole> roles = roleService.selectRolesByUserId(userId);
//        ajax.put("user", user);
//        ajax.put("roles", SysUser.isAdmin(userId) ? roles : roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
        return ajax;
    }

    /**
     * 分配服务工作
     */
    @RequiresPermissions("system:user:edit")
    @PutMapping("/allocWork")
    public AjaxResult updateWorkOfServer(Long userId, Long[] workIds)
    {
        ServerUser serverUser = serverUserService.selectServerUserById(userId);
        serverUserService.updateWorkOfServer(serverUser.getServerId(),workIds);
        return success();
    }
}
