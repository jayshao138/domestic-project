package com.ruoyi.domestic.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domestic.domain.Demand;
import com.ruoyi.domestic.service.IDemandService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 需求Controller
 * 
 * @author jayerListen
 * @date 2023-04-10
 */
@RestController
@RequestMapping("/demand")
public class DemandController extends BaseController
{
    @Autowired
    private IDemandService demandService;

    /**
     * 查询需求列表
     */
    @RequiresPermissions("domestic:demand:list")
    @GetMapping("/list")
    public TableDataInfo list(Demand demand)
    {
        startPage();
        List<Demand> list = demandService.selectDemandList(demand);
        return getDataTable(list);
    }

    /**
     * 导出需求列表
     */
    @RequiresPermissions("domestic:demand:export")
    @Log(title = "需求", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Demand demand)
    {
        List<Demand> list = demandService.selectDemandList(demand);
        ExcelUtil<Demand> util = new ExcelUtil<Demand>(Demand.class);
        util.exportExcel(response, list, "需求数据");
    }

    /**
     * 获取需求详细信息
     */
    @RequiresPermissions("domestic:demand:query")
    @GetMapping(value = "/{demandId}")
    public AjaxResult getInfo(@PathVariable("demandId") Long demandId)
    {
        return success(demandService.selectDemandByDemandId(demandId));
    }

    /**
     * 新增需求
     */
    @RequiresPermissions("domestic:demand:add")
    @Log(title = "需求", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Demand demand)
    {
        return toAjax(demandService.insertDemand(demand));
    }

    /**
     * 修改需求
     */
    @RequiresPermissions("domestic:demand:edit")
    @Log(title = "需求", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Demand demand)
    {
        return toAjax(demandService.updateDemand(demand));
    }

    /**
     * 删除需求
     */
    @RequiresPermissions("domestic:demand:remove")
    @Log(title = "需求", businessType = BusinessType.DELETE)
	@DeleteMapping("/{demandIds}")
    public AjaxResult remove(@PathVariable Long[] demandIds)
    {
        return toAjax(demandService.deleteDemandByDemandIds(demandIds));
    }
}
