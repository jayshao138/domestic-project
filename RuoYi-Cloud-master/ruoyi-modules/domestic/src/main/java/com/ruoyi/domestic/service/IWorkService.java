package com.ruoyi.domestic.service;

import java.util.List;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.domestic.domain.vo.TreeSelect;
import com.ruoyi.system.api.domain.SysDept;

/**
 * 服务Service接口
 *
 * @author JayerListen
 * @date 2023-04-03
 */
public interface IWorkService
{
    /**
     * 查询服务
     *
     * @param workId 服务主键
     * @return 服务
     */
    public Work selectWorkByWorkId(Long workId);

    /**
     * 查询服务列表
     *
     * @param work 服务
     * @return 服务集合
     */
    public List<Work> selectWorkList(Work work);

    /**
     * 新增服务
     *
     * @param work 服务
     * @return 结果
     */
    public int insertWork(Work work);

    /**
     * 修改服务
     *
     * @param work 服务
     * @return 结果
     */
    public int updateWork(Work work);

    /**
     * 批量删除服务
     *
     * @param workIds 需要删除的服务主键集合
     * @return 结果
     */
    public int deleteWorkByWorkIds(Long[] workIds);

    /**
     * 删除服务信息
     *
     * @param workId 服务主键
     * @return 结果
     */
    public int deleteWorkByWorkId(Long workId);

    /**
     * 查询部门树结构信息
     *
     * @param work 服务信息
     * @return 部门树信息集合
     */
    public List<TreeSelect> selectWorkTreeList(Work work);

    /**
     * 构建前端所需要树结构
     *
     * @param works 服务列表
     * @return 树结构列表
     */
    public List<Work> buildWorkTree(List<Work> works);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param works 服务列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildDeptTreeSelect(List<Work> works);
}
