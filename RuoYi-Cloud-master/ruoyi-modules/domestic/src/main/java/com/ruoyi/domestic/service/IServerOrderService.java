package com.ruoyi.domestic.service;

import com.ruoyi.domestic.domain.Order;

import java.util.List;

public interface IServerOrderService {
    public List<Order> selectServerOrderByServerId(Order order);
    public int confirmOrder(Long orderId);
    public int startWorkOfOrder(Long orderId);
    public int endWorkOfOrder(Long orderId);
    public int cancelOrder(Long orderId);
}
