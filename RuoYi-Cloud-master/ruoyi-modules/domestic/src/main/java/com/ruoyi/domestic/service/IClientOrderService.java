package com.ruoyi.domestic.service;

import com.ruoyi.domestic.domain.Order;

import java.util.List;

public interface IClientOrderService {

    public List<Order> selectOrderByClientId(Order order);

    public int cancelOrder(Long orderId);

    public int payOrder(Long orderId);

    public int evaluateOrder(Order order);


}
