package com.ruoyi.domestic.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 订单-家政服务对象 t_order
 *
 * @author jayerListen
 * @date 2023-04-10
 */
public class Order extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long orderId;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private Long orderNumber;

    /** 客户id */
    private Long clientId;

    /** 客户姓名 */
    private String clientName;

    /** 客户昵称 */
    @Excel(name = "客户昵称")
    private String clientNickname;

    /** 客户电话 */
    private String clientPhone;

    /** 服务人id */
    private Long serverId;

    /** 服务人姓名 */
    private String serverName;

    /** 服务人昵称 */
    @Excel(name = "服务人昵称")
    private String serverNickname;

    /** 服务人电话 */
    private String serverPhone;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private Integer status;

    /** 工作地点 */
    private String workAddress;

    /** 订单花费 */
    private Long orderCost;

    /** 订单评价 */
    private Integer orderEvaluation;

    /** 评价内容 */
    private String orderEvaluationContent;

    /** 服务类型id */
    private Long workId;

    /** 服务类型名 */
    @Excel(name = "服务类型名")
    private String workName;

    /** 发起时间 */
    private Date startTime;

    /** 完成时间 */
    private Date finishTime;

    public String getOrderEvaluationContent() {
        return orderEvaluationContent;
    }

    public void setOrderEvaluationContent(String orderEvaluationContent) {
        this.orderEvaluationContent = orderEvaluationContent;
    }

    public void setOrderId(Long orderId)
    {
        this.orderId = orderId;
    }

    public Long getOrderId()
    {
        return orderId;
    }
    public void setOrderNumber(Long orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public Long getOrderNumber()
    {
        return orderNumber;
    }
    public void setClientId(Long clientId)
    {
        this.clientId = clientId;
    }

    public Long getClientId()
    {
        return clientId;
    }
    public void setClientName(String clientName)
    {
        this.clientName = clientName;
    }

    public String getClientName()
    {
        return clientName;
    }
    public void setClientNickname(String clientNickname)
    {
        this.clientNickname = clientNickname;
    }

    public String getClientNickname()
    {
        return clientNickname;
    }
    public void setClientPhone(String clientPhone)
    {
        this.clientPhone = clientPhone;
    }

    public String getClientPhone()
    {
        return clientPhone;
    }
    public void setServerId(Long serverId)
    {
        this.serverId = serverId;
    }

    public Long getServerId()
    {
        return serverId;
    }
    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public String getServerName()
    {
        return serverName;
    }
    public void setServerNickname(String serverNickname)
    {
        this.serverNickname = serverNickname;
    }

    public String getServerNickname()
    {
        return serverNickname;
    }
    public void setServerPhone(String serverPhone)
    {
        this.serverPhone = serverPhone;
    }

    public String getServerPhone()
    {
        return serverPhone;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setWorkAddress(String workAddress)
    {
        this.workAddress = workAddress;
    }

    public String getWorkAddress()
    {
        return workAddress;
    }
    public void setOrderCost(Long orderCost)
    {
        this.orderCost = orderCost;
    }

    public Long getOrderCost()
    {
        return orderCost;
    }
    public void setOrderEvaluation(Integer orderEvaluation)
    {
        this.orderEvaluation = orderEvaluation;
    }

    public Integer getOrderEvaluation()
    {
        return orderEvaluation;
    }
    public void setWorkId(Long workId)
    {
        this.workId = workId;
    }

    public Long getWorkId()
    {
        return workId;
    }
    public void setWorkName(String workName)
    {
        this.workName = workName;
    }

    public String getWorkName()
    {
        return workName;
    }
    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public Date getStartTime()
    {
        return startTime;
    }
    public void setFinishTime(Date finishTime)
    {
        this.finishTime = finishTime;
    }

    public Date getFinishTime()
    {
        return finishTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("orderId", getOrderId())
            .append("orderNumber", getOrderNumber())
            .append("clientId", getClientId())
            .append("clientName", getClientName())
            .append("clientNickname", getClientNickname())
            .append("clientPhone", getClientPhone())
            .append("serverId", getServerId())
            .append("serverName", getServerName())
            .append("serverNickname", getServerNickname())
            .append("serverPhone", getServerPhone())
            .append("status", getStatus())
            .append("workAddress", getWorkAddress())
            .append("orderCost", getOrderCost())
            .append("orderEvaluation", getOrderEvaluation())
            .append("workId", getWorkId())
            .append("workName", getWorkName())
            .append("startTime", getStartTime())
            .append("finishTime", getFinishTime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
