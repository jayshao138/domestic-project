package com.ruoyi.domestic.service.impl;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.mapper.ClientOrderMapper;
import com.ruoyi.domestic.mapper.OrderMapper;
import com.ruoyi.domestic.service.IClientOrderService;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class ClientOrderServiceImpl implements IClientOrderService {
    @Autowired
    private ClientOrderMapper clientOrderMapper;
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Order> selectOrderByClientId(Order order) {
        LoginUser userMe = SecurityUtils.getLoginUser();
        order.setClientId(userMe.getUserid());
        List<Order> orders = clientOrderMapper.selectOrderByClientId(order);
        return orders;
    }

    @Override
    public int cancelOrder(Long orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        order.setStatus(0);
        return orderMapper.updateOrder(order);
    }

    @Override
    public int payOrder(Long orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        order.setStatus(5);
        return orderMapper.updateOrder(order);
    }

    @Override
    public int evaluateOrder(Order order) {
        order.setStatus(6);
        return orderMapper.updateOrder(order);
    }

}
