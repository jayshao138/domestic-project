package com.ruoyi.domestic.mapper;

import com.ruoyi.domestic.domain.DataShower;
import com.ruoyi.domestic.domain.Order;

/**
 * @author JayerListen
 * @create 2023-04-15 2023/4/15
 */
public interface DataShowMapper {
    public Long selectCountOfOrder();
    public Long selectCountOfEnterprise();
    public Long selectCountOfUser();
    public Long selectCountOfUndoOrder();

    public Long selectCountOfClientCancelOrder(Long clientId);
    public Long selectCountOfClientFinishOrder(Long clientId);
    public Long selectCountOfClientAllOrder(Long clientId);
    public Long selectCountOfClientWaitAcceptOrder(Long clientId);

    public Long selectCountOfServerFinishOrder(Long serverId);
    public Long selectCountOfServerWaitAcceptOrder(Long serverId);
    public Long selectCountOfServerUndoOrder(Long serverId);
    public Long selectCountOfServerAllOrder(Long serverId);
}
