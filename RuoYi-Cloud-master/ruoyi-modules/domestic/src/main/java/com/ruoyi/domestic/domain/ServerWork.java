package com.ruoyi.domestic.domain;

/**
 * @author JayerListen
 * @create 2023-04-05 2023/4/5
 */
public class ServerWork {
    private Long workId;
    private Long serverId;

    public Long getWorkId() {
        return workId;
    }

    public void setWorkId(Long workId) {
        this.workId = workId;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    @Override
    public String toString() {
        return "ServerWork{" +
                "workId=" + workId +
                ", serverId=" + serverId +
                '}';
    }
}
