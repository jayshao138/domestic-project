package com.ruoyi.domestic.mapper;

import com.ruoyi.domestic.domain.Demand;

import java.util.List;

public interface ServerDemandMapper {

    /**
     * 查询需求管理列表
     *
     * @param demand 需求管理
     * @return 需求管理集合
     */
    public List<Demand> selectServerDemandList(Demand demand);

    /**
     * 修改需求管理
     *
     * @param demand 需求管理
     * @return 结果
     */
    public int updateServerDemand(Demand demand);

    public List<Demand> selectDemandByServerId(Demand demand);

}
