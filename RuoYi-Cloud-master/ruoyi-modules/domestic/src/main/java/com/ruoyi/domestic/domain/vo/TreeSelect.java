package com.ruoyi.domestic.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.system.api.domain.SysDept;

import javax.swing.tree.TreePath;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author JayerListen
 * @create 2023-04-04 2023/4/4
 */
public class TreeSelect implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 节点ID */
    private Long id;

    /** 节点名称 */
    private String label;

    /** 子节点 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeSelect> children;

    public TreeSelect(){ }

    public TreeSelect(Work work)
    {
        this.id = work.getWorkId();
        this.label = work.getWorkName();
        this.children = ((List<Work>)work.getChildren()).stream().map(TreeSelect::new).collect(Collectors.toList());
    }




    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public List<TreeSelect> getChildren()
    {
        return children;
    }

    public void setChildren(List<TreeSelect> children)
    {
        this.children = children;
    }
}

