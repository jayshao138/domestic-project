package com.ruoyi.domestic.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.core.utils.DateUtils;
import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.domestic.domain.vo.TreeSelect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.domestic.mapper.WorkMapper;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.domestic.service.IWorkService;

/**
 * 服务Service业务层处理
 *
 * @author JayerListen
 * @date 2023-04-03
 */
@Service
public class WorkServiceImpl implements IWorkService
{
    @Autowired
    private WorkMapper workMapper;

    /**
     * 查询服务
     *
     * @param workId 服务主键
     * @return 服务
     */
    @Override
    public Work selectWorkByWorkId(Long workId)
    {
        return workMapper.selectWorkByWorkId(workId);
    }

    /**
     * 查询服务列表
     *
     * @param work 服务
     * @return 服务
     */
    @Override
    public List<Work> selectWorkList(Work work)
    {
        return workMapper.selectWorkList(work);
    }

    /**
     * 新增服务
     *
     * @param work 服务
     * @return 结果
     */
    @Override
    public int insertWork(Work work)
    {
        if(work.getParentId()==0){
            work.setAncestors(work.getParentId().toString());
        }else{
            Work info=workMapper.selectWorkByWorkId(work.getParentId());
            work.setAncestors(info.getAncestors()+","+work.getParentId());
        }
        work.setCreateTime(DateUtils.getNowDate());
        return workMapper.insertWork(work);
    }

    /**
     * 修改服务
     *
     * @param work 服务
     * @return 结果
     */
    @Override
    public int updateWork(Work work)
    {
        work.setUpdateTime(DateUtils.getNowDate());
        return workMapper.updateWork(work);
    }

    /**
     * 批量删除服务
     *
     * @param workIds 需要删除的服务主键
     * @return 结果
     */
    @Override
    public int deleteWorkByWorkIds(Long[] workIds)
    {
        return workMapper.deleteWorkByWorkIds(workIds);
    }

    /**
     * 删除服务信息
     *
     * @param workId 服务主键
     * @return 结果
     */
    @Override
    public int deleteWorkByWorkId(Long workId)
    {
        return workMapper.deleteWorkByWorkId(workId);
    }

    @Override
    public List<TreeSelect> selectWorkTreeList(Work work) {
        List<Work> works = selectWorkList(work);
        return buildDeptTreeSelect(works);
    }

    @Override
    public List<TreeSelect> buildDeptTreeSelect(List<Work> works) {
        List<Work> workTrees = buildWorkTree(works);
        return workTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    @Override
    public List<Work> buildWorkTree(List<Work> works) {
        List<Work> returnList = new ArrayList<Work>();
        List<Long> tempList = works.stream().map(Work::getWorkId).collect(Collectors.toList());
        for (Work work : works)
        {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(work.getParentId()))
            {
                recursionFn(works, work);
                returnList.add(work);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = works;
        }
        return returnList;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<Work> list, Work t)
    {
        // 得到子节点列表
        List<Work> childList = getChildList(list, t);
        t.setChildren(childList);
        for (Work tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<Work> getChildList(List<Work> list, Work t)
    {
        List<Work> tlist = new ArrayList<Work>();
        Iterator<Work> it = list.iterator();
        while (it.hasNext())
        {
            Work n = (Work) it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getWorkId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<Work> list, Work t)
    {
        return getChildList(list, t).size() > 0 ? true : false;
    }


}
