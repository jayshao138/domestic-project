package com.ruoyi.domestic.service;

/**
 * @author JayerListen
 * @create 2023-04-07 2023/4/7
 */
public interface IClientSubmitDemandService {
    public int submitDemand(Long workId,String workName,String demandDetail);
}
