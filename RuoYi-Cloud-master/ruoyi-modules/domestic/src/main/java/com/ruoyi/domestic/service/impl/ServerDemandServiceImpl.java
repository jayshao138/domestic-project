package com.ruoyi.domestic.service.impl;

import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.domestic.domain.Demand;
import com.ruoyi.domestic.domain.ServerUser;
import com.ruoyi.domestic.mapper.ServerDemandMapper;
import com.ruoyi.domestic.mapper.ServerUserMapper;
import com.ruoyi.domestic.service.IServerDemandService;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServerDemandServiceImpl implements IServerDemandService {
    @Autowired
    private ServerDemandMapper serverDemandMapper;
    @Override
    public List<Demand> selectDemandList(Demand demand) {
        demand.setStatus(1);    //只找待接收的
        return serverDemandMapper.selectServerDemandList(demand);
    }

    @Override
    public int updateDemand(Long demandId) {
        Demand newDemand=new Demand();
        LoginUser userMe = SecurityUtils.getLoginUser();


        newDemand.setServerId(userMe.getUserid());
        newDemand.setServerName(userMe.getUsername());
        newDemand.setServerNickname(userMe.getSysUser().getNickName());
        newDemand.setServerPhone(userMe.getSysUser().getPhonenumber());
        //设置需求状态为2-已接受
        newDemand.setStatus(2);

        newDemand.setDemandId(demandId);
        int a = serverDemandMapper.updateServerDemand(newDemand);
        System.out.println(a);
        System.out.println(newDemand);
        return a;

    }

    @Override
    public List<Demand> selectDemandByServerId(Demand demand)
    {
        Demand newDemand=new Demand();
        LoginUser userMe = SecurityUtils.getLoginUser();
        newDemand.setServerId(userMe.getUserid());
        newDemand.setStatus(2);
        return serverDemandMapper.selectDemandByServerId(newDemand);
    }

}
