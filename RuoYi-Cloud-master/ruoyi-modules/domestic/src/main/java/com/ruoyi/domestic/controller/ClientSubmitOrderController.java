package com.ruoyi.domestic.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.domain.vo.SubmitOrderVo;
import com.ruoyi.domestic.service.IClientSubmitOrderService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author JayerListen
 * @create 2023-04-09 2023/4/9
 */
@RestController
@RequestMapping("/clientSubmitOrder")
public class ClientSubmitOrderController extends BaseController {
    @Autowired
    private IClientSubmitOrderService clientSubmitOrderService;
    /**
     * 用户提交订单
     */
    @RequiresPermissions("domestic:submitOrder:submit")
    @PostMapping
    public AjaxResult submitOrder(@RequestBody SubmitOrderVo submitOrderVo)
    {
        System.out.println(submitOrderVo);
        return toAjax(clientSubmitOrderService.submitOrder(submitOrderVo));
    }
}
