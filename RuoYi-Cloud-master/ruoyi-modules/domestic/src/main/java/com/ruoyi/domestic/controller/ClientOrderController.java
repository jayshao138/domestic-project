package com.ruoyi.domestic.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.alibaba.csp.sentinel.slots.block.RuleConstant;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRule;
import com.alibaba.csp.sentinel.slots.block.flow.FlowRuleManager;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.mapper.OrderMapper;
import com.ruoyi.domestic.service.IClientOrderService;
import com.ruoyi.domestic.service.impl.ClientOrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/clientOrder")
public class ClientOrderController extends BaseController {
    @Autowired
    private IClientOrderService clientOrderService;


    /**
     * 获取订单-家政服务详细信息
     */
//    @RequiresPermissions("domestic:order:query")
    @SentinelResource(value = "selectOrderByClientId", blockHandler = "selectOrderByClientIdBlockHandler", fallback = "selectOrderByClientIdFallback")
    @RequiresPermissions("domestic:clientOrder:list")
    @GetMapping("/list")
    public TableDataInfo list(Order order)
    {
        startPage();
        List<Order> list = clientOrderService.selectOrderByClientId(order);
        return getDataTable(list);
    }

    /**
     * 修改订单-家政服务 取消
     */
    @RequiresPermissions("domestic:clientOrder:edit")
    @Log(title = "订单-家政服务", businessType = BusinessType.UPDATE)
    @GetMapping(value = "/cancelOrder/{orderId}")
    public AjaxResult cancelOrder(@PathVariable("orderId")Long orderId)
    {
        return toAjax(clientOrderService.cancelOrder(orderId));
    }

    /**
     * 修改订单-家政服务 付款
     */
    @RequiresPermissions("domestic:clientOrder:edit")
    @Log(title = "订单-家政服务", businessType = BusinessType.UPDATE)
    @GetMapping(value = "/payOrder/{orderId}")
    public AjaxResult payOrder(@PathVariable("orderId")Long orderId)
    {
        return toAjax(clientOrderService.payOrder(orderId));
    }

    /**
     * 修改订单-家政服务 评价
     */
    @RequiresPermissions("domestic:clientOrder:edit")
    @Log(title = "订单-家政服务", businessType = BusinessType.UPDATE)
    @GetMapping(value = "/evaluateOrder")
    public AjaxResult evaluateOrder(Order order)
    {
        return toAjax(clientOrderService.evaluateOrder(order));
    }

    // 服务流量控制处理，参数最后多一个 BlockException，其余与原函数一致(返回类型，参数!!!!!!!!)
    public TableDataInfo selectOrderByClientIdBlockHandler(Order order, BlockException ex)
    {
        System.out.println("selectUserByNameBlockHandler异常信息：" + ex.getMessage());
        List<String> exString=new ArrayList<>();
        exString.add("{\"code\":\"500\",\"msg\": \"" + "服务流控处理\"}");
        return getDataTable(exString);
    }

    // 服务熔断降级处理，函数签名与原函数一致或加一个 Throwable 类型的参数
    public Object selectOrderByClientIdFallback(String username, Throwable throwable)
    {
        System.out.println("selectUserByNameFallback异常信息：" + throwable.getMessage());
        return "{\"code\":\"500\",\"msg\": \"" + username + "服务熔断降级处理\"}";
    }


    @PostConstruct
    private void initFlowQpsRule() {
        List<FlowRule> rules = new ArrayList<>();
        FlowRule rule = new FlowRule("selectOrderByClientId");
        // set limit qps to 20
        rule.setCount(500);
        rule.setGrade(RuleConstant.FLOW_GRADE_QPS);
        rule.setLimitApp("default");
        rules.add(rule);
        FlowRuleManager.loadRules(rules);
    }
}
