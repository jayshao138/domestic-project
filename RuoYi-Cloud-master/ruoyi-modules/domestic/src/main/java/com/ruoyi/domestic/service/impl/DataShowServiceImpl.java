package com.ruoyi.domestic.service.impl;

import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.domestic.domain.DataShower;
import com.ruoyi.domestic.mapper.DataShowMapper;
import com.ruoyi.domestic.service.DataShowService;
import com.ruoyi.system.api.model.LoginUser;
import org.antlr.v4.parse.ResyncToEndOfRuleBlock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author JayerListen
 * @create 2023-04-15 2023/4/15
 */
@Service
public class DataShowServiceImpl implements DataShowService {
    @Autowired
    private DataShowMapper dataShowMapper;

    @Override
    public DataShower datashow() {
        DataShower dataShower = new DataShower();
        dataShower.setIcon_data_corporation(dataShowMapper.selectCountOfEnterprise());
        dataShower.setIcon_data_user(dataShowMapper.selectCountOfUser());
        dataShower.setIcon_data_nodingdan(dataShowMapper.selectCountOfUndoOrder());
        dataShower.setIcon_data_shop(dataShowMapper.selectCountOfOrder());
        return dataShower;
    }

    @Override
    public DataShower datashowOfClient() {
        LoginUser userMe = SecurityUtils.getLoginUser();


        DataShower dataShower = new DataShower();
        dataShower.setIcon_data_yiquxiaodingdan(dataShowMapper.selectCountOfClientCancelOrder(userMe.getUserid()));
        dataShower.setIcon_data_yifabudingdan(dataShowMapper.selectCountOfClientFinishOrder(userMe.getUserid()));
        dataShower.setIcon_data_alldingdan(dataShowMapper.selectCountOfClientAllOrder(userMe.getUserid()));
        dataShower.setIcon_data_daijieshoudingdan(dataShowMapper.selectCountOfClientWaitAcceptOrder(userMe.getUserid()));
        return dataShower;
    }

    @Override
    public DataShower datashowOfServer() {
        LoginUser userMe = SecurityUtils.getLoginUser();


        DataShower dataShower = new DataShower();
        dataShower.setIcon_data_daijieshoudingdan(dataShowMapper.selectCountOfServerWaitAcceptOrder(userMe.getUserid()));
        dataShower.setIcon_data_finishdingdan(dataShowMapper.selectCountOfServerFinishOrder(userMe.getUserid()));
        dataShower.setIcon_data_nodingdan(dataShowMapper.selectCountOfServerUndoOrder(userMe.getUserid()));
        dataShower.setIcon_data_alldingdan(dataShowMapper.selectCountOfServerAllOrder(userMe.getUserid()));
        return dataShower;
    }
}
