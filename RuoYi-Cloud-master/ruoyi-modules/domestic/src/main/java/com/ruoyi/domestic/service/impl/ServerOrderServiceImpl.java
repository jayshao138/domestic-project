package com.ruoyi.domestic.service.impl;

import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.mapper.OrderMapper;
import com.ruoyi.domestic.mapper.ServerOrderMapper;
import com.ruoyi.domestic.service.IServerOrderService;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ServerOrderServiceImpl implements IServerOrderService {
    @Autowired
    private ServerOrderMapper serverOrderMapper;
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Order> selectServerOrderByServerId(Order order) {
        LoginUser userMe = SecurityUtils.getLoginUser();
        //System.out.println(userMe.getUserid());
        //Order order = new Order();
        order.setServerId(userMe.getUserid());
        List<Order> orders = serverOrderMapper.selectServerOrderByServerId(order);
        return orders;
    }

    @Override
    public int confirmOrder(Long orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        order.setStatus(2);
        return orderMapper.updateOrder(order);
    }

    @Override
    public int startWorkOfOrder(Long orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        order.setStatus(3);
        order.setStartTime(new Date());
        return orderMapper.updateOrder(order);
    }

    @Override
    public int endWorkOfOrder(Long orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        order.setStatus(4);
        order.setFinishTime(new Date());
        return orderMapper.updateOrder(order);
    }

    @Override
    public int cancelOrder(Long orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        order.setStatus(0);
        return orderMapper.updateOrder(order);
    }
}
