package com.ruoyi.domestic.mapper;

import com.ruoyi.domestic.domain.Order;

import java.util.List;

public interface ServerOrderMapper {
    public List<Order> selectServerOrderByServerId(Order order);
}
