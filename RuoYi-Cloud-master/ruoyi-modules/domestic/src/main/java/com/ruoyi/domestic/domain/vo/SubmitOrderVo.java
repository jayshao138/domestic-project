package com.ruoyi.domestic.domain.vo;

public class SubmitOrderVo {
    private Long serverId;
    private String serverName;
    private String serverNickname;
    private String serverPhone;

    private String workAddress;

    private Long workId;
    private String workName;

    public String getServerPhone() {
        return serverPhone;
    }

    public void setServerPhone(String serverPhone) {
        this.serverPhone = serverPhone;
    }

    public Long getServerId() {
        return serverId;
    }

    public void setServerId(Long serverId) {
        this.serverId = serverId;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerNickname() {
        return serverNickname;
    }

    public void setServerNickname(String serverNickname) {
        this.serverNickname = serverNickname;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public Long getWorkId() {
        return workId;
    }

    public void setWorkId(Long workId) {
        this.workId = workId;
    }

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    @Override
    public String toString() {
        return "SubmitOrderVo{" +
                "serverId=" + serverId +
                ", serverName='" + serverName + '\'' +
                ", serverNickname='" + serverNickname + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", workId=" + workId +
                ", workName='" + workName + '\'' +
                '}';
    }
}
