package com.ruoyi.domestic.domain;

import com.ruoyi.system.api.domain.SysDept;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.TreeEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * 服务对象 t_work
 *
 * @author JayerListen
 * @date 2023-04-03
 */
public class Work extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 服务项目id */
    private Long workId;

    /** 服务名称 */
    @Excel(name = "服务名称")
    private String workName;

    /** 负责人 */
    private String leader;

    /** 联系电话 */
    private String phone;

    /** 邮箱 */
    private String email;

    /** 服务图片 */
    private String image;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer workSort;

    /** 删除标志 */
    private String delFlag;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setWorkId(Long workId)
    {
        this.workId = workId;
    }

    public Long getWorkId()
    {
        return workId;
    }
    public void setWorkName(String workName)
    {
        this.workName = workName;
    }

    public String getWorkName()
    {
        return workName;
    }
    public void setLeader(String leader)
    {
        this.leader = leader;
    }

    public String getLeader()
    {
        return leader;
    }
    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getPhone()
    {
        return phone;
    }
    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        return email;
    }
    public void setWorkSort(Integer workSort)
    {
        this.workSort = workSort;
    }

    public Integer getWorkSort()
    {
        return workSort;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return "Work{" +
                "workId=" + workId +
                ", workName='" + workName + '\'' +
                ", leader='" + leader + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", image='" + image + '\'' +
                ", workSort=" + workSort +
                ", delFlag='" + delFlag + '\'' +
                ", status=" + status +
                '}';
    }
}
