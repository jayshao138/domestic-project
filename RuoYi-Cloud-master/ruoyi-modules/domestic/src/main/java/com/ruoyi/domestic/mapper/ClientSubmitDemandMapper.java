package com.ruoyi.domestic.mapper;

import com.ruoyi.domestic.domain.Demand;

/**
 * @author JayerListen
 * @create 2023-04-07 2023/4/7
 */
public interface ClientSubmitDemandMapper {
    /**
     * 用户发布需求
     *
     * @param demand 需求demain
     * @return 结果
     */
    public int insertDemand(Demand demand);
}
