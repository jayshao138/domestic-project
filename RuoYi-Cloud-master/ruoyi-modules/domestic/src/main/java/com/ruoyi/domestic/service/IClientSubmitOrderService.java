package com.ruoyi.domestic.service;

import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.domestic.domain.vo.SubmitOrderVo;
import com.ruoyi.system.api.domain.SysUser;

/**
 * @author JayerListen
 * @create 2023-04-07 2023/4/7
 */
public interface IClientSubmitOrderService {
//    public int submitOrder(Long serverId,String serverName,String workAddress,Long orderCost,Long workId,String workName);
    public int submitOrder(SubmitOrderVo submitOrderVo);

}