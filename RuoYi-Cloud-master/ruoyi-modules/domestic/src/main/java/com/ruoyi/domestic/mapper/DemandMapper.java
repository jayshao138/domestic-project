package com.ruoyi.domestic.mapper;

import java.util.List;
import com.ruoyi.domestic.domain.Demand;

/**
 * 需求Mapper接口
 * 
 * @author jayerListen
 * @date 2023-04-10
 */
public interface DemandMapper 
{
    /**
     * 查询需求
     * 
     * @param demandId 需求主键
     * @return 需求
     */
    public Demand selectDemandByDemandId(Long demandId);

    /**
     * 查询需求列表
     * 
     * @param demand 需求
     * @return 需求集合
     */
    public List<Demand> selectDemandList(Demand demand);

    /**
     * 新增需求
     * 
     * @param demand 需求
     * @return 结果
     */
    public int insertDemand(Demand demand);

    /**
     * 修改需求
     * 
     * @param demand 需求
     * @return 结果
     */
    public int updateDemand(Demand demand);

    /**
     * 删除需求
     * 
     * @param demandId 需求主键
     * @return 结果
     */
    public int deleteDemandByDemandId(Long demandId);

    /**
     * 批量删除需求
     * 
     * @param demandIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDemandByDemandIds(Long[] demandIds);
}
