package com.ruoyi.domestic.mapper;

import com.ruoyi.domestic.domain.ServerUser;
import com.ruoyi.domestic.domain.ServerWork;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.domestic.domain.vo.ServerUserVo;
import com.ruoyi.system.api.domain.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author JayerListen
 * @create 2023-04-03 2023/4/3
 */
public interface ServerUserMapper {
    /**
     * 根据条件分页查询服务人列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<ServerUserVo> selectUserList(@Param("user") SysUser user, @Param("workId") int workId);
    public int insertServerUser(@Param("user") SysUser user);
    public ServerUser selectServerUserById(Long userId);
    public List<Work> selectWorksOfServer(Long serverId);
    public int deleteServerWorkByServerId(Long serverId);
    public int batchServerWork(List<ServerWork> serverWorkList);
}
