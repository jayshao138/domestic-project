package com.ruoyi.domestic.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 需求对象 t_demand
 * 
 * @author jayerListen
 * @date 2023-04-10
 */
public class Demand extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 需求id */
    @Excel(name = "需求id")
    private Long demandId;

    /** 用户id */
    private Long userId;

    /** 用户名称 */
    private String userName;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String userNickname;

    /** 用户手机号 */
    private String userPhone;

    /** 服务人id */
    private Long serverId;

    /** 服务人姓名 */
    private String serverName;

    /** 服务人昵称 */
    @Excel(name = "服务人昵称")
    private String serverNickname;

    /** 服务人手机号 */
    private String serverPhone;

    /** 服务类型id */
    private Long workId;

    /** 服务类型 */
    @Excel(name = "服务类型")
    private String workName;

    /** 服务需求 */
    private String demandDetail;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    public void setDemandId(Long demandId) 
    {
        this.demandId = demandId;
    }

    public Long getDemandId() 
    {
        return demandId;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserNickname(String userNickname) 
    {
        this.userNickname = userNickname;
    }

    public String getUserNickname() 
    {
        return userNickname;
    }
    public void setUserPhone(String userPhone) 
    {
        this.userPhone = userPhone;
    }

    public String getUserPhone() 
    {
        return userPhone;
    }
    public void setServerId(Long serverId) 
    {
        this.serverId = serverId;
    }

    public Long getServerId() 
    {
        return serverId;
    }
    public void setServerName(String serverName) 
    {
        this.serverName = serverName;
    }

    public String getServerName() 
    {
        return serverName;
    }
    public void setServerNickname(String serverNickname) 
    {
        this.serverNickname = serverNickname;
    }

    public String getServerNickname() 
    {
        return serverNickname;
    }
    public void setServerPhone(String serverPhone) 
    {
        this.serverPhone = serverPhone;
    }

    public String getServerPhone() 
    {
        return serverPhone;
    }
    public void setWorkId(Long workId) 
    {
        this.workId = workId;
    }

    public Long getWorkId() 
    {
        return workId;
    }
    public void setWorkName(String workName) 
    {
        this.workName = workName;
    }

    public String getWorkName() 
    {
        return workName;
    }
    public void setDemandDetail(String demandDetail) 
    {
        this.demandDetail = demandDetail;
    }

    public String getDemandDetail() 
    {
        return demandDetail;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("demandId", getDemandId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("userNickname", getUserNickname())
            .append("userPhone", getUserPhone())
            .append("serverId", getServerId())
            .append("serverName", getServerName())
            .append("serverNickname", getServerNickname())
            .append("serverPhone", getServerPhone())
            .append("workId", getWorkId())
            .append("workName", getWorkName())
            .append("demandDetail", getDemandDetail())
            .append("status", getStatus())
            .toString();
    }
}
