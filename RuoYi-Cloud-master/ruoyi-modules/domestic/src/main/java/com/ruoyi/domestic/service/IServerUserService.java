package com.ruoyi.domestic.service;

import com.ruoyi.domestic.domain.ServerUser;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.domestic.domain.vo.ServerUserVo;
import com.ruoyi.system.api.domain.SysUser;

import java.util.List;

/**
 * @author JayerListen
 * @create 2023-04-03 2023/4/3
 */
public interface IServerUserService {

    public List<ServerUserVo> selectUserList(SysUser user, int workId);

    public int insertServer(SysUser user);

    public ServerUser selectServerUserById(Long userId);
    public List<Work> selectWorksOfServer(Long serverId);
    public void updateWorkOfServer(Long serverId,Long[] workIds);
}
