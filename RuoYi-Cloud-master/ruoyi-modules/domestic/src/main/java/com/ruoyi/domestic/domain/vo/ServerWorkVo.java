package com.ruoyi.domestic.domain.vo;

import com.ruoyi.domestic.domain.Work;
import com.ruoyi.system.api.domain.SysUser;

import java.util.List;

/**
 * @author JayerListen
 * @create 2023-04-05 2023/4/5
 */
public class ServerWorkVo {
    private SysUser sysUser;
    private List<Work> works;

    public SysUser getSysUser() {
        return sysUser;
    }

    public void setSysUser(SysUser sysUser) {
        this.sysUser = sysUser;
    }

    public List<Work> getWorks() {
        return works;
    }

    public void setWorks(List<Work> works) {
        this.works = works;
    }

    @Override
    public String toString() {
        return "ServerWorkVo{" +
                "sysUser=" + sysUser +
                ", works=" + works +
                '}';
    }
}
