package com.ruoyi.domestic.controller;

import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.utils.file.FileTypeUtils;
import com.ruoyi.common.core.utils.file.MimeTypeUtils;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.system.api.RemoteFileService;
import com.ruoyi.system.api.domain.SysFile;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.domestic.service.IWorkService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import org.springframework.web.multipart.MultipartFile;

/**
 * 服务Controller
 *
 * @author JayerListen
 * @date 2023-04-03
 */
@RestController
@RequestMapping("/work")
public class WorkController extends BaseController
{
    @Autowired
    private IWorkService workService;

    @Autowired
    private RemoteFileService remoteFileService;

    /**
     * 查询服务列表
     */
    @RequiresPermissions("domestic:work:list")
    @GetMapping("/list")
    public AjaxResult list(Work work)
    {
        List<Work> list = workService.selectWorkList(work);
        return success(list);
    }

    /**
     * 导出服务列表
     */
    @RequiresPermissions("domestic:work:export")
    @Log(title = "服务", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, Work work)
    {
        List<Work> list = workService.selectWorkList(work);
        ExcelUtil<Work> util = new ExcelUtil<Work>(Work.class);
        util.exportExcel(response, list, "服务数据");
    }

    /**
     * 获取服务详细信息
     */
    @RequiresPermissions("domestic:work:query")
    @GetMapping(value = "/{workId}")
    public AjaxResult getInfo(@PathVariable("workId") Long workId)
    {
        return success(workService.selectWorkByWorkId(workId));
    }

    /**
     * 新增服务
     */
    @RequiresPermissions("domestic:work:add")
    @Log(title = "服务", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Work work)
    {
        return toAjax(workService.insertWork(work));
    }

    /**
     * 修改服务
     */
    @RequiresPermissions("domestic:work:edit")
    @Log(title = "服务", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Work work)
    {
        return toAjax(workService.updateWork(work));
    }

    /**
     * 删除服务
     */
    @RequiresPermissions("domestic:work:remove")
    @Log(title = "服务", businessType = BusinessType.DELETE)
	@DeleteMapping("/{workIds}")
    public AjaxResult remove(@PathVariable Long[] workIds)
    {
        return toAjax(workService.deleteWorkByWorkIds(workIds));
    }

    /**
     * 头像上传
     */
    @RequiresPermissions("domestic:work:edit")
    @PostMapping("/image/{workId}")
    public AjaxResult image(@PathVariable("workId") Long workId,@RequestParam("workImage") MultipartFile workImage)
    {
        if (!workImage.isEmpty())
        {
            LoginUser loginUser = SecurityUtils.getLoginUser();
            String extension = FileTypeUtils.getExtension(workImage);
            if (!StringUtils.equalsAnyIgnoreCase(extension, MimeTypeUtils.IMAGE_EXTENSION))
            {
                return error("文件格式不正确，请上传" + Arrays.toString(MimeTypeUtils.IMAGE_EXTENSION) + "格式");
            }
            R<SysFile> fileResult = remoteFileService.upload(workImage);
            if (StringUtils.isNull(fileResult) || StringUtils.isNull(fileResult.getData()))
            {
                return error("文件服务异常，请联系管理员");
            }
            String url = fileResult.getData().getUrl();
            Work work = new Work();
            work.setWorkId(workId);
            work.setImage(url);
            if (workService.updateWork(work)>0)
            {
                AjaxResult ajax = AjaxResult.success();
                ajax.put("workImage", url);
                // 更新缓存用户头像
//                loginUser.getSysUser().setAvatar(url);
//                tokenService.setLoginUser(loginUser);
                return ajax;
            }
        }
        return error("上传图片异常，请联系管理员");
    }
}
