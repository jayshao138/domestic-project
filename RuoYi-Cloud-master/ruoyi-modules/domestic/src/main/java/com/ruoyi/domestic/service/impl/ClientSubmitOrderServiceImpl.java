package com.ruoyi.domestic.service.impl;

import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.domain.Work;
import com.ruoyi.domestic.domain.vo.SubmitOrderVo;
import com.ruoyi.domestic.mapper.ClientSubmitOrderMapper;
import com.ruoyi.domestic.service.IClientSubmitOrderService;
import com.ruoyi.system.api.domain.SysUser;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Security;
import java.util.Date;

/**
 * @author JayerListen
 * @create 2023-04-09 2023/4/9
 */
@Service
public class ClientSubmitOrderServiceImpl implements IClientSubmitOrderService {

    @Autowired
    private ClientSubmitOrderMapper clientSubmitOrderMapper;

    //客户提交订单
    @Override
    public int submitOrder(SubmitOrderVo submitOrderVo) {
        Order newOrder=new Order();
        LoginUser userMe = SecurityUtils.getLoginUser();
        //设置本人信息
        newOrder.setClientId(userMe.getUserid());
        newOrder.setClientName(userMe.getUsername());
        newOrder.setClientNickname(userMe.getSysUser().getNickName());
        newOrder.setClientPhone(userMe.getSysUser().getPhonenumber());
        //设置服务人id和姓名
        newOrder.setServerId(submitOrderVo.getServerId());
        newOrder.setServerName(submitOrderVo.getServerName());
        newOrder.setServerNickname(submitOrderVo.getServerNickname());
        newOrder.setServerPhone(submitOrderVo.getServerPhone());
        //设置服务类型和服务名
        newOrder.setWorkId(submitOrderVo.getWorkId());
        newOrder.setWorkName(submitOrderVo.getWorkName());

        //设置工作地点
        newOrder.setWorkAddress(submitOrderVo.getWorkAddress());
        //设置需求状态为0-待接受
        newOrder.setStatus(1);

        //生成订单号
        long time = new Date().getTime();
        String info = newOrder.getClientId()+
                      newOrder.getClientName()+
                      newOrder.getClientNickname()+
                      newOrder.getClientPhone()+
                      newOrder.getServerId()+
                      newOrder.getServerName()+
                      newOrder.getServerNickname()+
                      newOrder.getServerPhone()+
                      newOrder.getWorkId()+
                      newOrder.getWorkName();
        newOrder.setOrderNumber(new Long(time+Math.abs(info.hashCode())));

        int i = clientSubmitOrderMapper.insertOrder(newOrder);
        return i;
    }
}
