package com.ruoyi.domestic.service;

import com.ruoyi.domestic.domain.Demand;

import java.util.List;

public interface IClientDemandService {

    public List<Demand> selectDemandByClientId(Demand demand);

    public int cancelDemand(Long demandId);

    public int affirmDemand(Demand demand);

}
