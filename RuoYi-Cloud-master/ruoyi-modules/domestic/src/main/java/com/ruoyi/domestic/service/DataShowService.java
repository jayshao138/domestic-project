package com.ruoyi.domestic.service;

import com.ruoyi.domestic.domain.DataShower;

/**
 * @author JayerListen
 * @create 2023-04-15 2023/4/15
 */
public interface DataShowService {
    public DataShower datashow();
    public DataShower datashowOfClient();
    public DataShower datashowOfServer();
}
