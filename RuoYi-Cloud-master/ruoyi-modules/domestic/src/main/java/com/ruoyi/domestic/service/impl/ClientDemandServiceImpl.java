package com.ruoyi.domestic.service.impl;

import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.domestic.domain.Demand;
//import com.ruoyi.domestic.mapper.ClientOrderMapper;
import com.ruoyi.domestic.domain.Order;
import com.ruoyi.domestic.domain.vo.SubmitOrderVo;
import com.ruoyi.domestic.mapper.DemandMapper;
import com.ruoyi.domestic.service.IClientDemandService;
import com.ruoyi.domestic.service.IClientSubmitDemandService;
import com.ruoyi.domestic.service.IClientSubmitOrderService;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ClientDemandServiceImpl implements IClientDemandService {

    @Autowired
    private DemandMapper demandMapper;

    @Autowired
    private IClientSubmitOrderService clientSubmitOrderService;

    @Override
    public List<Demand> selectDemandByClientId(Demand demand) {
        LoginUser userMe = SecurityUtils.getLoginUser();
        demand.setUserId(userMe.getUserid());
        List<Demand> demands = demandMapper.selectDemandList(demand);
        return demands;
    }

    @Override
    public int cancelDemand(Long demandId) {
        Demand demand = new Demand();
        demand.setDemandId(demandId);
        demand.setStatus(0);
        return demandMapper.updateDemand(demand);
    }

    @Override
    public int affirmDemand(Demand demand) {
//        Demand demand = new Demand();
        demand.setStatus(3);
        int i = demandMapper.updateDemand(demand);

        //自动下单
        SubmitOrderVo submitOrderVo=new SubmitOrderVo();
        submitOrderVo.setServerId(demand.getServerId());
        submitOrderVo.setServerName(demand.getServerName());
        submitOrderVo.setServerNickname(demand.getServerNickname());
        submitOrderVo.setServerPhone(demand.getServerPhone());
        submitOrderVo.setWorkId(demand.getWorkId());
        submitOrderVo.setWorkName(demand.getWorkName());
        submitOrderVo.setWorkAddress("(需求已说明)");
        int j = clientSubmitOrderService.submitOrder(submitOrderVo);
        return i*j;
    }




}
