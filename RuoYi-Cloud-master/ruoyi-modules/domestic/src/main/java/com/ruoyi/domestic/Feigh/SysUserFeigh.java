package com.ruoyi.domestic.Feigh;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.system.api.domain.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author JayerListen
 * @create 2023-04-05 2023/4/5
 */
@FeignClient(name="ruoyi-system",path="/user")
public interface SysUserFeigh {
    @PostMapping("/server")
    SysUser insertServer(@Validated @RequestBody SysUser user);

    @GetMapping("/server/{userId}")
    public SysUser getUser(@PathVariable("userId") Long userId);
}
