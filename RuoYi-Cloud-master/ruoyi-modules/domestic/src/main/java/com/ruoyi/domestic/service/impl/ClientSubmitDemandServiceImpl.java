package com.ruoyi.domestic.service.impl;

import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.domestic.domain.Demand;
import com.ruoyi.domestic.mapper.ClientSubmitDemandMapper;
import com.ruoyi.domestic.service.IClientSubmitDemandService;
import com.ruoyi.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Security;

/**
 * @author JayerListen
 * @create 2023-04-07 2023/4/7
 */
@Service
public class ClientSubmitDemandServiceImpl implements IClientSubmitDemandService {

    @Autowired
    private ClientSubmitDemandMapper clientSubmitDemandMapper;

    //客户发布需求
    @Override
    public int submitDemand(Long workId,String workName,String demandDetail) {
        Demand newDemand=new Demand();
        LoginUser userMe = SecurityUtils.getLoginUser();
        //设置本人信息
        newDemand.setUserId(userMe.getUserid());
        newDemand.setUserName(userMe.getUsername());
        newDemand.setUserNickname(userMe.getSysUser().getNickName());
        newDemand.setUserPhone(userMe.getSysUser().getPhonenumber());
        //设置服务项目
        newDemand.setWorkId(workId);
        newDemand.setWorkName(workName);
        //设置需求细节
        newDemand.setDemandDetail(demandDetail);
        //设置需求状态为1-待接受
        newDemand.setStatus(1);

        int i = clientSubmitDemandMapper.insertDemand(newDemand);
        return i;
    }
}
