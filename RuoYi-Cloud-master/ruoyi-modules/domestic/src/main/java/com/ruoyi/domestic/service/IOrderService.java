package com.ruoyi.domestic.service;

import java.util.List;
import com.ruoyi.domestic.domain.Order;

/**
 * 订单-家政服务Service接口
 * 
 * @author jayerListen
 * @date 2023-04-10
 */
public interface IOrderService 
{
    /**
     * 查询订单-家政服务
     * 
     * @param orderId 订单-家政服务主键
     * @return 订单-家政服务
     */
    public Order selectOrderByOrderId(Long orderId);

    /**
     * 查询订单-家政服务列表
     * 
     * @param order 订单-家政服务
     * @return 订单-家政服务集合
     */
    public List<Order> selectOrderList(Order order);

    /**
     * 新增订单-家政服务
     * 
     * @param order 订单-家政服务
     * @return 结果
     */
    public int insertOrder(Order order);

    /**
     * 修改订单-家政服务
     * 
     * @param order 订单-家政服务
     * @return 结果
     */
    public int updateOrder(Order order);

    /**
     * 批量删除订单-家政服务
     * 
     * @param orderIds 需要删除的订单-家政服务主键集合
     * @return 结果
     */
    public int deleteOrderByOrderIds(Long[] orderIds);

    /**
     * 删除订单-家政服务信息
     * 
     * @param orderId 订单-家政服务主键
     * @return 结果
     */
    public int deleteOrderByOrderId(Long orderId);
}
