package com.ruoyi.domestic.controller;

import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.web.page.TableDataInfo;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.domestic.domain.Demand;
import com.ruoyi.domestic.service.IClientDemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/clientDemand")
public class ClientDemandController extends BaseController {
    @Autowired
    private IClientDemandService clientDemandService;


    /**
     * 获取需求-家政服务详细信息
     */
//    @RequiresPermissions("domestic:order:query")
    @RequiresPermissions("domestic:clientDemand:list")
    @GetMapping("/list")
    public TableDataInfo list(Demand demand)
    {
        startPage();
        List<Demand> list = clientDemandService.selectDemandByClientId(demand);
        return getDataTable(list);
    }

    /**
     * 修改需求-家政服务 取消
     */
    @RequiresPermissions("domestic:clientDemand:edit")
    @Log(title = "需求-家政服务", businessType = BusinessType.UPDATE)
    @GetMapping(value = "/cancelDemand/{demandId}")
    public AjaxResult cancelDemand(@PathVariable("demandId")Long demandId)
    {
        return toAjax(clientDemandService.cancelDemand(demandId));
    }

    /**
     * 修改需求-家政服务 确认
     */
    @RequiresPermissions("domestic:clientDemand:edit")
    @Log(title = "需求-家政服务", businessType = BusinessType.UPDATE)
    @GetMapping(value = "/affirmDemand")
    public AjaxResult affirmDemand(Demand demand)
    {
        return toAjax(clientDemandService.affirmDemand(demand));
    }
}
