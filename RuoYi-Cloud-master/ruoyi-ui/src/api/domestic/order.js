import request from '@/utils/request'

// 查询订单-家政服务列表
export function listOrder(query) {
  return request({
    url: '/domestic/order/list',
    method: 'get',
    params: query
  })
}

// 查询订单-家政服务详细
export function getOrder(orderId) {
  return request({
    url: '/domestic/order/' + orderId,
    method: 'get'
  })
}

// 新增订单-家政服务
export function addOrder(data) {
  return request({
    url: '/domestic/order',
    method: 'post',
    data: data
  })
}

// 修改订单-家政服务
export function updateOrder(data) {
  return request({
    url: '/domestic/order',
    method: 'put',
    data: data
  })
}

// 删除订单-家政服务
export function delOrder(orderId) {
  return request({
    url: '/domestic/order/' + orderId,
    method: 'delete'
  })
}
