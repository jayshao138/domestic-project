import request from '@/utils/request'

// 查询需求列表
export function listDemand(query) {
  return request({
    url: '/domestic/demand/list',
    method: 'get',
    params: query
  })
}

// 查询需求详细
export function getDemand(demandId) {
  return request({
    url: '/domestic/demand/' + demandId,
    method: 'get'
  })
}

// 新增需求
export function addDemand(data) {
  return request({
    url: '/domestic/demand',
    method: 'post',
    data: data
  })
}

// 修改需求
export function updateDemand(data) {
  return request({
    url: '/domestic/demand',
    method: 'put',
    data: data
  })
}

// 删除需求
export function delDemand(demandId) {
  return request({
    url: '/domestic/demand/' + demandId,
    method: 'delete'
  })
}
