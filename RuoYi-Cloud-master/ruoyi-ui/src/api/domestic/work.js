import request from '@/utils/request'

// 查询服务列表
export function listWork(query) {
  return request({
    url: '/domestic/work/list',
    method: 'get',
    params: query
  })
}

// 查询服务详细
export function getWork(workId) {
  return request({
    url: '/domestic/work/' + workId,
    method: 'get'
  })
}

// 新增服务
export function addWork(data) {
  return request({
    url: '/domestic/work',
    method: 'post',
    data: data
  })
}

// 修改服务
export function updateWork(data) {
  return request({
    url: '/domestic/work',
    method: 'put',
    data: data
  })
}

// 删除服务
export function delWork(workId) {
  return request({
    url: '/domestic/work/' + workId,
    method: 'delete'
  })
}

// 服务图片上传
export function uploadImage(workId,data) {
  return request({
    url: '/domestic/work/image/'+ workId,
    method: 'post',
    data: data
  })
}
