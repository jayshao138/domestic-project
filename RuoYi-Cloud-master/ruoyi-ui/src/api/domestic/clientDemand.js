import request from '@/utils/request'

//客户发布需求
export function submitDemand(data) {
  return request({
    url: '/domestic/clientSubmitDemand',
    method: 'post',
    data: data
  })
}

// 查询需求列表
export function getMyListDemand(query) {
  return request({
    url: '/domestic/clientDemand/list',
    method: 'get',
    params: query
  })
}

// 确认需求
export function confirmMyDemand(query) {
  return request({
    url: '/domestic/clientDemand/affirmDemand/',
    method: 'get',
    params: query
  })
}

// 取消需求
export function cancelMyDemand(demandId) {
  return request({
    url: '/domestic/clientDemand/cancelDemand/' + demandId,
    method: 'get'
  })
}
