import request from '@/utils/request'

// 员工查询订单表
export function serverGetMyOrder(query) {
  return request({
    url: '/domestic/serverOrder/getMyOrders',
    method: 'get',
    params: query
  })
}

// 员工接受订单
export function serverAcceptMyOrder(orderId) {
  return request({
    url: '/domestic/serverOrder/confirmOrder/' + orderId,
    method: 'post'
  })
}

// 员工开始订单
export function serverStartMyOrder(orderId) {
  return request({
    url: '/domestic/serverOrder/startWork/' + orderId,
    method: 'post'
  })
}

// 完成订单
export function serverEndMyOrder(orderId) {
  return request({
    url: '/domestic/serverOrder/endWork/' + orderId,
    method: 'post'
  })
}

// 取消订单
export function serverCancelMyOrder(orderId) {
  return request({
    url: '/domestic/serverOrder/cancelOrder/' + orderId,
    method: 'post'
  })
}
