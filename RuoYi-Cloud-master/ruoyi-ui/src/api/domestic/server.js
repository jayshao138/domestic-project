import request from '@/utils/request'

// 查询部门下拉树结构
export function workTreeSelect() {
  return request({
    url: '/domestic/server/workTree',
    method: 'get'
  })
}

// 查询服务者列表
export function listServer(query) {
  return request({
    url: '/domestic/server/list',
    method: 'get',
    params: query
  })
}

// 新增服务者
export function addServer(data) {
  return request({
    url: '/domestic/server',
    method: 'post',
    data: data
  })
}

// 查询授权角色
export function getWorkOfServer(userId) {
  return request({
    url: '/domestic/server/allocWork/' + userId,
    method: 'get'
  })
}

// 保存授权角色
export function updateWorkOfServer(data) {
  return request({
    url: '/domestic/server/allocWork',
    method: 'put',
    params: data
  })
}