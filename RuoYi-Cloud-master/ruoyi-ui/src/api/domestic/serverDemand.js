import request from '@/utils/request'

// 查询需求列表
export function listDemand(query) {
  return request({
    url: '/domestic/serverdemand/list',
    method: 'get',
    params: query
  })
}

  // 确认需求
export function confirmMyDemand(demandId) {
  return request({
    url: '/domestic/serverdemand/recieveDemand/' + demandId,
    method: 'post'
  })
}

export function listMyDemand(query) {
  return request({
    url: '/domestic/serverdemand/mylist',
    method: 'get',
    params: query
  })
}
