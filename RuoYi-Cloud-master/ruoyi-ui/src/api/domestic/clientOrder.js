import request from '@/utils/request'

//客户下订单
export function submitOrder(data) {
  return request({
    url: '/domestic/clientSubmitOrder',
    method: 'post',
    data: data
  })
}

// 客户查询订单表
export function clientGetMyOrder(query) {
  return request({
    url: '/domestic/clientOrder/list',
    method: 'get',
    params: query
  })
}

// 客户取消订单表
export function clientCancelOrder(orderId) {
  return request({
    url: '/domestic/clientOrder/cancelOrder/' + orderId,
    method: 'get'
  })
}

// 客户支付订单表
export function clientPayOrder(orderId) {
  return request({
    url: '/domestic/clientOrder/payOrder/' + orderId,
    method: 'get'
  })
}

// 客户评价订单表
export function clientEvaluateOrder(query) {
  return request({
    url: '/domestic/clientOrder/evaluateOrder',
    method: 'get',
    params: query
  })
}
